﻿using Microsoft.EntityFrameworkCore;
using state.model.Entities;
using System;
using System.Collections.Generic;

namespace state.model
{
    public class StateDbContext : DbContext, IStateDbContext
    {
        public DbSet<Building> Buildings { get; set; }

        public DbSet<BuildingPowerInfo> BuildingPowerInfos { get; set; }


        public DbSet<Organization> Organizations { get; set; }


        public void InsertBulk(List<Organization> list)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
            Organizations.AddRange(list);
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.TrackAll;
            ChangeTracker.AutoDetectChangesEnabled = true;
            SaveChanges();
        }

        public StateDbContext(DbContextOptions<StateDbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
