﻿using System;
using System.Collections.Generic;
using System.Text;

namespace state.model.Entities
{
    public class BuildingPowerInfo
    {
        public int Id { get; set; }

        public int BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public int Year { get; set; }

        public int Under20 { get; set; }

        public int More20 { get; set; }

        public int TotalUsing { get; set; }


    }
}
