﻿using System;
using System.Collections.Generic;
using System.Text;

namespace state.model.Entities
{
    public class Building
    {
        public int Id { get; set; }

        public string Address { get; set; }

        public int Lat { get; set; }

        public int Lng { get; set; }
    }
}
