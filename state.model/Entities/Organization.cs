﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace state.model.Entities
{
    public class Organization
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Edrpou { get; set; }

        public string Address { get; set; }

        public string  Boss { get; set; }

        public string Kved { get; set; }

        public string State { get; set; }
    }
}
