﻿using Microsoft.EntityFrameworkCore;
using state.model.Entities;
using System.Collections.Generic;

namespace state.model
{
    public interface IStateDbContext
    {
        DbSet<BuildingPowerInfo> BuildingPowerInfos { get; set; }
        DbSet<Building> Buildings { get; set; }
        DbSet<Organization> Organizations { get; set; }

        void InsertBulk(List<Organization> list);

        int SaveChanges();
    }
}