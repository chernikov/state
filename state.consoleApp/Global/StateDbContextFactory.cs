﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using state.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace state.consoleApp.Global
{
    class StateDbContextFactory : IDesignTimeDbContextFactory<StateDbContext>
    {
        public StateDbContext CreateDbContext(string[] args)
        {
            var configuration = ConfigurationManager.GetConfiguration();
            var options = new DbContextOptionsBuilder<StateDbContext>();
            options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            return new StateDbContext(options.Options);
        }
    }
}
