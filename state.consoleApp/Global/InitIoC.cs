﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using state.consoleApp.Processors;
using state.model;
using System;
using System.Collections.Generic;
using System.Text;

namespace state.consoleApp.Global
{
    public class InitIoC
    {
        public static IServiceProvider InitServiceProvider()
        {
            var services = new ServiceCollection();
         
            InitLogging(services);
            InitDb(services);
            InitProcessor(services);

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions
            {
                CaptureMessageTemplates = true,
                CaptureMessageProperties = true
            });

            return serviceProvider;
        }

        private static void InitLogging(IServiceCollection services)
        {
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddLogging(p => p.SetMinimumLevel(LogLevel.Trace));
        }

        private static void InitDb(ServiceCollection services)
        {
            var configuration = ConfigurationManager.GetConfiguration();
            services.AddDbContext<StateDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IStateDbContext, StateDbContext>();
        }

        private static void InitProcessor(ServiceCollection services)
        {
            services.AddTransient<OrganizationProcessor>();
        }

    }
}
