﻿
using Microsoft.EntityFrameworkCore;
using state.model;
using state.model.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state.consoleApp.Processors
{
    public class OrganizationProcessor : BaseXmlProcessor
    {

        //private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public OrganizationProcessor(IStateDbContext sqlContext) : base(sqlContext)
        {
        }

        public override void Process()
        {
            var cp1251 = Encoding.GetEncoding(1251);
            using (var reader = new StreamReader(@"data\\organization.xml", cp1251, true))
            {
                var sb = new StringBuilder();
                var collectMode = false;
                var i = 0;
                var list = new List<Organization>();
                while (reader.Peek() > 0)
                {
                    var row = reader.ReadLine();
                    if (row == "</ROW>")
                    {
                        collectMode = false;
                        i++;
                        var item = ProcessData(sb);
                        if (item != null)
                        {
                            list.Add(item);
                        }
                        sb = new StringBuilder();

                        if (i % 100 == 0 && list.Any())
                        {
                            SqlContext.Organizations.AddRange(list);
                            SqlContext.SaveChanges();
                            list.Clear();
                        }
                        if (i % 100 == 0)
                        {
                            //logger.Trace($"{i}");
                        }
                    }
                    if (collectMode)
                    {
                        sb.AppendLine(row);
                    }
                    if (row == "<ROW>")
                    {
                        collectMode = true;
                    }
                }
            }
        }

        private Organization ProcessData(StringBuilder sb)
        {
            try
            {
                var @string = sb.ToString();
                var lines = sb.ToString().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                var organization = new Organization()
                {
                    Name = GetInner(lines[0]),
                    ShortName = GetInner(lines[1]),
                    Edrpou = GetInner(lines[2]),
                    Address = GetInner(lines[3]),
                    Boss = GetInner(lines[4]),
                    Kved = GetInner(lines[5]),
                    State = GetInner(lines[6])
                };
                //logger.Debug($"{organization.Name} {organization.Address}");

                return organization;
            }
            catch (Exception ex)
            {
                //logger.Error(sb.ToString());
            }
            return null;
        }
    }
}
