﻿using state.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state.consoleApp.Processors
{
    public abstract class BaseProcessor
    {
      

        protected IStateDbContext SqlContext { get; set; }

        public BaseProcessor(IStateDbContext sqlContext)
        {
            SqlContext = sqlContext;
        }

        public abstract void Process();
    }
}
