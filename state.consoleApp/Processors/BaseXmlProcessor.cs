﻿using state.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace state.consoleApp.Processors
{
    public abstract class BaseXmlProcessor : BaseProcessor
    {
        public BaseXmlProcessor(IStateDbContext sqlContext) : base(sqlContext)
        {
        }

        protected string GetInner(string line)
        {
            var start = line.IndexOf(">");
            var end = line.IndexOf("</");

            var text = line.Substring(start + 1, end - start - 1);
            var result = HttpUtility.HtmlDecode(text);
            return result;
        }
    }
}
