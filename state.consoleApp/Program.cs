﻿using System;
using System.ComponentModel;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using state.consoleApp.Global;
using state.consoleApp.Processors;
using state.model;

namespace state.consoleApp
{
    class Program
    {
        static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var logger = NLog.LogManager.LoadConfiguration("nlog.config").GetCurrentClassLogger();
            try
            {

                _serviceProvider = InitIoC.InitServiceProvider();
                _serviceProvider = IoCSupport.InitContainer(serviceCollection);
                var runner = _serviceProvider.GetRequiredService<Runner>();
                runner.DoAction("Action1");

                var cp1251 = Encoding.GetEncoding(1251);
                var context = _serviceProvider.GetRequiredService<IStateDbContext>();

                var processor = _serviceProvider.GetRequiredService<OrganizationProcessor>();
                processor.Process();

                //processor.Process();

                Console.WriteLine("Press ANY key to exit");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                //NLog: catch setup errors
                logger.Error(ex, "Stopped program because of exception");
                    throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }


        private static IServiceProvider BuildDi(ServiceCollection services)
        {
            //Runner is the custom class
            services.AddTransient<Runner>();

            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Trace));

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions {
                CaptureMessageTemplates = true,
                CaptureMessageProperties = true
            });
            return serviceProvider;
        }
    }
}
